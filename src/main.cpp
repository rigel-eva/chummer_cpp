#include <iostream>
#include <fstream>
#include <string>//Technicly not PSU CS Lower division legal ... but personal porject, so idc
#include "rapidxml-1.13/rapidxml.hpp"
using namespace std;
#define FILELOC "/home/rigel/Google Drive/Character Sheets/Runnerhub/Babsí/Babsí.chum5"

void read_file(string& file_out);
int grab_attribute_value(rapidxml::xml_node<>* node);
void print_skills(rapidxml::xml_node<>* node, const char * title);
int main(){
    string chummer_xml="";
    rapidxml::xml_document<> omai;
    rapidxml::xml_node<> *node;
    rapidxml::xml_node<> *cNode;
    //Ok Step 2, reading the file
    read_file(chummer_xml);
    //And step 3, let's go aheand and have rapidxml parse the character
    omai.parse<0>((char*) chummer_xml.c_str());
    cNode=omai.first_node("character");
    //Grabbing the name
    node=cNode->first_node("name");
    cout<<"Name of character is: "<<node->value()<<endl;
    //Grabbing their aliases
    node=cNode->first_node("alias");
    cout<<"Alias: "<<node->value()<<endl;
    //And starting to grab their attributes!
    node=cNode->first_node("attributes")->first_node("attribute");
    //So, basic explanation time
    //     ⇙ This is checking to see if we have run out of siblings
    //               ⇙And this is itterating through our list of siblings      
    for(; node!=0; node=node->next_sibling()){
        //Ok let's go ahead and get our total first ...
        int attribute=grab_attribute_value(node);
        if(attribute>0){
            cout<<"Attribute: "<<node->first_node("name")->value()
                <<": "<<attribute<<"/"<<node->first_node("metatypemax")->value()
                <<endl;
        }
    }
    node = cNode->first_node("newskills")->first_node("groups")->first_node("group");
    print_skills(node, "Group");
    node = cNode->first_node("newskills")->first_node("skills")->first_node("skill");
    print_skills(node,"Skills");
    node = cNode->first_node("newskills")->first_node("knoskills")->first_node("skill");
    print_skills(node, "KnowSkill");
    return 0;
}
void read_file(string& file_out){
    //Right, setting up a few variables...
    ifstream chummer_file;
    string line;
    //Neat, let's go ahead and grab our files
    chummer_file.open(FILELOC);
    // And ... read it!
    if(chummer_file.is_open()){
        while(getline(chummer_file,line)){
            file_out+=line;
        }
        chummer_file.close();
    }
}
int grab_attribute_value(rapidxml::xml_node<>* node){
    //Slight note: we are making this work for both skills and attributes so ...
    int attribute=0;
    if(node->first_node("metatypemin")!=0){//If we can find this node, we want it.
        attribute+=atoi(node->first_node("metatypemin")->value());
    }   
    attribute+=atoi(node->first_node("base")->value());
    attribute+=atoi(node->first_node("karma")->value());
    return attribute;
}
void print_skills(rapidxml::xml_node<>* node, const char * title){
        for(; node!=0; node=node->next_sibling()){
        int attribute=grab_attribute_value(node);
        if(attribute>0){
            cout<<"\t"<<title<<": "<<node->first_node("name")->value()<<": "<<attribute<<endl;
        }
    }
}